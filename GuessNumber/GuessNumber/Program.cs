﻿using System;
using System.Linq;

namespace GuessNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                Console.WriteLine("-------------------------------");
            }
        }

        /// <summary>
        /// 開始遊戲
        /// </summary>
        static void Start()
        {
            //製作數字 0000 ~ 9999 且字元不可重複
            var list = Enumerable.Range(0, 10000).Select(number => number.ToString().PadLeft(4, '0'))
                                                 .Where(number => !HasRepeatDigit(number));

            //一次從集合前面取，一次從後面取，增加隨機率
            bool startFromFirst = true;
            while (true)
            {
                string currentNumber = startFromFirst ? list.FirstOrDefault() : list.LastOrDefault();
                startFromFirst = !startFromFirst;

                Console.WriteLine($"Guess is {currentNumber}，The number of candidates is {list.Count()}");
                Console.Write("Enter the number of A and B : ");
                var input = Console.ReadLine();
                var judgeResult = new JudgeResult
                {
                    A = Convert.ToInt32(input.Split(' ')[0]),
                    B = Convert.ToInt32(input.Split(' ')[1])
                };

                if (judgeResult.A == currentNumber.Length)
                {
                    break;
                }

                //「集合內的 GetJudgeResult(猜測數, 答案)」跟 「剛剛輸入的 GetJudgeResult(答案, 猜測數)」的結果應會一樣，藉此從集合篩出答案
                list = list.Where(number => currentNumber != number && judgeResult == GetJudgeResult(currentNumber, number));
            }
        }

        /// <summary>
        /// 檢查一組數字裡面是否相同字元出現超過 1 次
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        static bool HasRepeatDigit(string n)
        {
            foreach (var digit in n)
            {
                if(n.Count(i => i == digit) > 1)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 檢查兩數的 AB 比對結果
        /// </summary>
        /// <param name="n">第一個數</param>
        /// <param name="m">第二個數</param>
        /// <returns></returns>
        static JudgeResult GetJudgeResult(string n, string m)
        {
            var result = new JudgeResult();

            for (int i = 0; i < 4; i++)
            {
                if(n[i] == m[i])
                {
                    result.A++;
                    continue;
                }

                if (m.Contains(n[i]))
                {
                    result.B++;
                }
            }

            return result;
        }
    }

    /// <summary>
    /// AB 比對結果
    /// </summary>
    class JudgeResult
    {
        public int A { get; set; }
        public int B { get; set; }

        public static bool operator ==(JudgeResult a, JudgeResult b)
        {
            return a.A == b.A && a.B == b.B;
        }
        public static bool operator !=(JudgeResult a, JudgeResult b)
        {
            return !(a.A == b.A && a.B == b.B);
        }
    }
}

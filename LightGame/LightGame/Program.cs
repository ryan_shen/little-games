﻿using System;

namespace LightGame
{
    class Program
    {
        //input 地圖
        static bool[,] InputMap;
        //output 地圖
        static bool[,] OutputMap;
        //列數
        static int RowCount;
        //要點擊的格子的地圖
        static bool[,] ClickMap;
        //是否已找到解
        static bool isResultFound;

        static void Main(string[] args)
        {
            while (true)
            {
                Input();

                //開始暴力求解
                isResultFound = false;
                ClickMap = new bool[RowCount, RowCount];
                DFS(0, 0);

                Output();
            }
        }

        /// <summary>
        /// 取得輸入資料
        /// </summary>
        static void Input()
        {
            //輸入列數
            Console.Write("Enter row count of the square : ");
            RowCount = Convert.ToInt32(Console.ReadLine());

            //輸入地圖
            Console.WriteLine("Enter the map (1 = on，0 = off) : ");
            InputMap = new bool[RowCount, RowCount];
            for (int i = 0; i < RowCount; i++)
            {
                string row = Console.ReadLine();
                string[] cells = row.Split(' ');
                for (int j = 0; j < RowCount; j++)
                {
                    InputMap[i, j] = cells[j] == "1";
                }
            }
        }

        /// <summary>
        /// 輸出結果
        /// </summary>
        static void Output()
        {
            Console.WriteLine("The solution was found, the click map is : ");
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < RowCount; j++)
                {
                    Console.Write(Convert.ToInt32(ClickMap[i, j]));
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// 窮舉所有可能的情況
        /// </summary>
        /// <param name="rowLayer">第幾列</param>
        /// <param name="columnLayer">第幾欄</param>
        static void DFS(int rowLayer, int columnLayer)
        {
            for (int i = 0; i < 2; i++)
            {
                if (isResultFound)
                {
                    return;
                }

                //先設 false，再設 true
                ClickMap[rowLayer, columnLayer] = i == 1;

                if (columnLayer + 1 < RowCount)
                {
                    DFS(rowLayer, columnLayer + 1);
                }
                else if (rowLayer + 1 < RowCount)
                {
                    DFS(rowLayer + 1, 0);
                }
                else
                {
                    //開始執行測試
                    StartProcess();
                    if (Check())
                    {
                        isResultFound = true;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// 開始執行模擬測試
        /// </summary>
        static void StartProcess()
        {
            OutputMap = new bool[RowCount, RowCount];
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < RowCount; j++)
                {
                    OutputMap[i, j] = InputMap[i, j];
                }
            }

            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < RowCount; j++)
                {
                    if (ClickMap[i, j])
                    {
                        Click(i, j);
                    }
                }
            }
        }

        /// <summary>
        /// 點擊格子
        /// </summary>
        /// <param name="row">列號</param>
        /// <param name="column">行號</param>
        static void Click(int row, int column)
        {
            ReverseCell(row, column);
            ReverseCell(row, column + 1);
            ReverseCell(row, column - 1);
            ReverseCell(row + 1, column);
            ReverseCell(row - 1, column);
        }

        /// <summary>
        /// 把格子狀態反轉
        /// </summary>
        /// <param name="row">列號</param>
        /// <param name="column">行號</param>
        static void ReverseCell(int row, int column)
        {
            if (row < 0 || row >= RowCount || column < 0 || column >= RowCount)
            {
                return;
            }

            OutputMap[row, column] = !OutputMap[row, column];
        }

        /// <summary>
        /// 檢查模擬測試的結果是否通過
        /// </summary>
        /// <returns></returns>
        static bool Check()
        {
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < RowCount; j++)
                {
                    if (!OutputMap[i, j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}

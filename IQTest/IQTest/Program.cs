﻿using System;
using System.Linq;

namespace IQTest
{
    internal class Program
    {
        private static Question[] questions;

        private static void Main(string[] args)
        {
            questions = new Question[10];
            for (int i = 0; i < 10; i++)
            {
                questions[i] = new Question { Number = i + 1, Answer = 1 };
            }

            DFS(0);
            Console.ReadLine();
        }

        private static void DFS(int layer)
        {
            for (int i = 1; i <= 5; i++)
            {
                questions[layer].Answer = i;
                if (layer == questions.Length - 1)
                {
                    int sumOfPassed = 0;
                    foreach (Question q in questions)
                    {
                        bool result = Check(q.Number - 1);
                        if (result)
                        {
                            sumOfPassed++;
                        }
                    }

                    //找到解了
                    if (sumOfPassed == questions.Length)
                    {
                        PrintResult();
                    }
                }
                else
                {
                    DFS(layer + 1);
                }
            }
        }

        private static bool Check(int question)
        {
            switch (question)
            {
                case 0:
                    return questions.Count(x => x.Answer == 1) == questions[question].Answer;
                case 1:
                    return questions.Count(x => x.Answer == 2) == questions[question].Answer;
                case 2:
                    return questions.Count(x => x.Answer == 3) == questions[question].Answer;
                case 3:
                    return questions.Count(x => x.Answer == 4) == questions[question].Answer;
                case 4:
                    return questions.Count(x => x.Answer == 5) == questions[question].Answer;
                case 5:
                    int sumOfDifference1 = 0;
                    for (int i = 0; i < 9; i++)
                    {
                        if (Math.Abs(questions[i + 1].Answer - questions[i].Answer) == 1)
                        {
                            sumOfDifference1++;
                        }
                    }
                    return sumOfDifference1 == questions[question].Answer;
                case 6:
                    int sumOfGCDGreaterThan1 = 0;
                    for (int i = 0; i < 10; i++)
                    {
                        if (GCD(questions[i].Answer, questions[i].Number) > 1)
                        {
                            sumOfGCDGreaterThan1++;
                        }
                    }
                    return sumOfGCDGreaterThan1 == questions[question].Answer;
                case 7:
                    int sumOfAnswerGreaterThanNumber = 0;
                    for (int i = 0; i < 10; i++)
                    {
                        if (questions[i].Answer > questions[i].Number)
                        {
                            sumOfAnswerGreaterThanNumber++;
                        }
                    }
                    return sumOfAnswerGreaterThanNumber == questions[question].Answer;
                case 8:
                    return questions.Sum(x => x.Answer) % 6 == 0;
                case 9:
                    for (int i = 0; i < 9; i++)
                    {
                        for (int j = 1; j < 10; j++)
                        {
                            if (questions[i].Answer == questions[j].Answer)
                            {
                                if (!IsPrime(Math.Abs(questions[i].Number - questions[j].Number)))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    return true;
            }
            return false;
        }

        private static int GCD(int a, int b)
        {
            (a, b) = (Math.Max(a, b), Math.Min(a, b));
            while (b != 0)
            {
                (a, b) = (b, a % b);
            }
            return a;
        }

        private static bool IsPrime(int n)
        {
            for (int i = 2; i < n; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        private static void PrintResult()
        {
            foreach (Question q in questions)
            {
                Console.WriteLine($"Q{q.Number}: {q.Answer}");
            }
            Console.WriteLine("-----");
        }
    }

    internal class Question
    {
        /// <summary>
        /// 題號
        /// </summary>
        public int Number;

        /// <summary>
        /// 該題答案
        /// </summary>
        public int Answer;
    }
}
